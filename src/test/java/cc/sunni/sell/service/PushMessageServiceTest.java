package cc.sunni.sell.service;

import cc.sunni.sell.dto.OrderDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author jl
 * @since 2021/1/24 14:17
 */
@SpringBootTest
class PushMessageServiceTest {
    @Autowired
    private PushMessageService pushMessageService;
    @Autowired
    private OrderMasterService orderMasterService;

    @Test
    void orderStatus() {
        OrderDto orderDto = orderMasterService.findOne("1351204972359979008");
        pushMessageService.orderStatus(orderDto);
    }
}