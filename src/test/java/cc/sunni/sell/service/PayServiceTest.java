package cc.sunni.sell.service;

import cc.sunni.sell.dto.OrderDto;
import cc.sunni.sell.entity.OrderMaster;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author jl
 * @since 2021/1/20 21:06
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
class PayServiceTest {
    @Autowired
    private PayService payService;
    @Autowired
    private OrderMasterService orderMasterService;

    @Test
    void create() {
        OrderDto orderDto = orderMasterService.findOne("1351204972359979008");
        payService.create(orderDto);
    }

    @Test
    void update() {
        LambdaUpdateWrapper<OrderMaster> wrapper = new LambdaUpdateWrapper<>();
        wrapper.set(OrderMaster::getBuyerName,null);
        wrapper.set(OrderMaster::getBuyerAddress,"北京");
        wrapper.eq(OrderMaster::getOrderId,"1353365210974523392");
        orderMasterService.update(null,wrapper);
    }
}