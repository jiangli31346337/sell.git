package cc.sunni.sell.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品类目
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Data
@TableName("product_category")
public class ProductCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
    @TableId
    private Integer categoryId;
	/**
	 * 类目名字
	 */
    private String categoryName;
	/**
	 * 类目编号
	 */
    private Integer categoryType;
	/**
	 * 创建时间
	 */
    private Date createTime;
	/**
	 * 修改时间
	 */
    private Date updateTime;

}
