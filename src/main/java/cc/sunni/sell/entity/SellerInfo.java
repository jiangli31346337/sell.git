package cc.sunni.sell.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * 卖家信息表
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Data
@TableName("seller_info")
public class SellerInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
	/**
	 * 用户名
	 */
    private String username;
	/**
	 * 密码
	 */
	@JsonIgnore // 查询不会显示该属性,对象序列化时忽略该属性
    private String password;
	/**
	 * 微信openid
	 */
    private String openid;
	/**
	 * 创建时间
	 */
    private Date createTime;
	/**
	 * 修改时间
	 */
    private Date updateTime;

}
