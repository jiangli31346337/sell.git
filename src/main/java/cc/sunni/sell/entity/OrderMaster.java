package cc.sunni.sell.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 订单表
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Data
@TableName("order_master")
public class OrderMaster implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
    @TableId(type = IdType.ASSIGN_ID)
    private String orderId;
	/**
	 * 买家名字
	 */
    private String buyerName;
	/**
	 * 买家电话
	 */
    private String buyerPhone;
	/**
	 * 买家地址
	 */
    private String buyerAddress;
	/**
	 * 买家微信openid
	 */
    private String buyerOpenid;
	/**
	 * 订单总金额
	 */
    private BigDecimal orderAmount;
	/**
	 * 订单状态, 默认为新下单
	 */
    private Integer orderStatus;
	/**
	 * 支付状态, 默认未支付
	 */
    private Integer payStatus;
	/**
	 * 创建时间
	 */
    private Date createTime;
	/**
	 * 修改时间
	 */
    private Date updateTime;

}
