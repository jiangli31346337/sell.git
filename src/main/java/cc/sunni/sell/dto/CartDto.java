package cc.sunni.sell.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jl
 * @since 2021/1/18 20:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartDto {

    private String productId;

    private Integer productQuantity;
}
