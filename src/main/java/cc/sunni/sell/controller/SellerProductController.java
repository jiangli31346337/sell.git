package cc.sunni.sell.controller;

import cc.sunni.sell.entity.ProductCategory;
import cc.sunni.sell.entity.ProductInfo;
import cc.sunni.sell.exception.RRException;
import cc.sunni.sell.form.ProductForm;
import cc.sunni.sell.service.ProductCategoryService;
import cc.sunni.sell.service.ProductInfoService;
import cc.sunni.sell.utils.PageUtils;
import cn.hutool.core.util.StrUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * 卖家端商品
 *
 * @author jl
 * @since 2021/1/21 19:22
 */
@Controller
@RequestMapping("/seller/product")
public class SellerProductController {
    @Autowired
    private ProductInfoService productService;
    @Autowired
    private ProductCategoryService categoryService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public ModelAndView list(@RequestParam(value = "page", defaultValue = "1") Integer page,
                             @RequestParam(value = "size", defaultValue = "10") Integer size,
                             Map<String, Object> map) {
        PageUtils pageUtils = productService.queryPage(page, size, map);
        map.put("productInfoPage", pageUtils);
        map.put("currentPage", page);
        map.put("size", size);
        return new ModelAndView("product/list", map);
    }

    /**
     * 商品上架
     */
    @RequestMapping("/on_sale")
    public ModelAndView onSale(@RequestParam("productId") String productId, Map<String, Object> map) {
        map.put("url", "/sell/seller/product/list");
        try {
            productService.onSale(productId);
        } catch (RRException e) {
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error", map);
        }
        return new ModelAndView("common/success", map);
    }

    /**
     * 商品下架
     */
    @RequestMapping("/off_sale")
    public ModelAndView offSale(@RequestParam("productId") String productId, Map<String, Object> map) {
        map.put("url", "/sell/seller/product/list");
        try {
            productService.offSale(productId);
        } catch (RRException e) {
            map.put("msg", e.getMessage());
            return new ModelAndView("common/error", map);
        }
        return new ModelAndView("common/success", map);
    }

    /**
     * 现在商品
     */
    @GetMapping("/index")
    public ModelAndView index(@RequestParam(value = "productId", required = false) String productId, Map<String, Object> map) {
        if (StrUtil.isNotBlank(productId)) {
            ProductInfo productInfo = productService.getById(productId);
            map.put("productInfo", productInfo);
        }
        //查询所有的类目
        List<ProductCategory> categoryList = categoryService.list();
        map.put("categoryList", categoryList);
        return new ModelAndView("product/index", map);
    }

    /**
     * 商品新增/修改
     */
    @PostMapping("/save")
    @CacheEvict(cacheNames = "product#7200",allEntries=true)
    public ModelAndView save(@Validated ProductForm form, BindingResult bindingResult, Map<String, Object> map) {
        if (bindingResult.hasErrors()) {
            map.put("msg", bindingResult.getFieldError().getDefaultMessage());
            map.put("url", "/sell/seller/product/index");
            return new ModelAndView("common/error", map);
        }

        ProductInfo productInfo = new ProductInfo();
        try {
            //如果productId为空, 说明是新增
            if (StrUtil.isNotBlank(form.getProductId())) {
                productInfo = productService.getById(form.getProductId());
                BeanUtils.copyProperties(form, productInfo);
                productService.updateById(productInfo);
            } else {
                BeanUtils.copyProperties(form, productInfo);
                productService.save(productInfo);
            }
        } catch (RRException e) {
            map.put("msg", e.getMessage());
            map.put("url", "/sell/seller/product/index");
            return new ModelAndView("common/error", map);
        }

        map.put("url", "/sell/seller/product/list");
        return new ModelAndView("common/success", map);
    }

}
