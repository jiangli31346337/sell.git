package cc.sunni.sell.config;

import com.lly835.bestpay.config.WxPayConfig;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 2017-07-04 01:05
 */
@Component
public class WeChatPayConfig {

    @Bean
    public BestPayServiceImpl bestPayService() {
        BestPayServiceImpl bestPayService = new BestPayServiceImpl();
        // 微信支付配置
        WxPayConfig wxPayConfig = new WxPayConfig();
        wxPayConfig.setAppId("wx6ad144e54af67d87");
        wxPayConfig.setMchId("1483469312");
        wxPayConfig.setMchKey("T6m9iK73b0kn9g5v426MKfHQH7X8rKwb");
        wxPayConfig.setNotifyUrl("https://www.sunni.cc");
        bestPayService.setWxPayConfig(wxPayConfig);

        return bestPayService;
    }

}
