package cc.sunni.sell.utils;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.Map;

/**
 * 查询参数
 * @author jcc52
 */
public class Query<T> {
    /**
     * 当前页码
     */
    public static final String PAGE_NO = "pageNo";
    /**
     * 每页显示记录数
     */
    public static final String SIZE = "size";

    public IPage<T> getPage(Map<String, Object> params) {
        //分页参数
        long pageNo = 1;
        long size = 10;
        if (params.get(PAGE_NO) != null) {
            pageNo = Long.parseLong(String.valueOf(params.get(PAGE_NO)));
        }
        if (params.get(SIZE) != null) {
            size = Long.parseLong(String.valueOf(params.get(SIZE)));
        }
        //分页对象
        return new Page<>(pageNo, size);
    }
}