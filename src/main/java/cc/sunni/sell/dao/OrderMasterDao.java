package cc.sunni.sell.dao;

import cc.sunni.sell.entity.OrderMaster;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单表
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Mapper
public interface OrderMasterDao extends BaseMapper<OrderMaster> {
	
}
