package cc.sunni.sell.dao;

import cc.sunni.sell.dto.ProductDto;
import cc.sunni.sell.entity.ProductInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 商品表
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Mapper
public interface ProductInfoDao extends BaseMapper<ProductInfo> {

    int decrStock(@Param("productId") String productId, @Param("quantity") Integer quantity);

    int incrStock(@Param("productId") String productId, @Param("quantity") Integer quantity);

    List<ProductDto> queryPage(Page<ProductDto> page, Map<String, Object> map);
}
