package cc.sunni.sell.dao;

import cc.sunni.sell.entity.SellerInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 卖家信息表
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Mapper
public interface SellerInfoDao extends BaseMapper<SellerInfo> {
	
}
