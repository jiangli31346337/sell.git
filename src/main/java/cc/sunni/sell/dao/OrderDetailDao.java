package cc.sunni.sell.dao;

import cc.sunni.sell.entity.OrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单详情
 * 
 * @author jl
 * @since 2021-01-17 19:27:13
 */
@Mapper
public interface OrderDetailDao extends BaseMapper<OrderDetail> {
	
}
