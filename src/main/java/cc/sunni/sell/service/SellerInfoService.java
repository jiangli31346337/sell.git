package cc.sunni.sell.service;

import cc.sunni.sell.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import cc.sunni.sell.entity.SellerInfo;
import java.util.Map;

/**
 * 卖家信息表
 *
 * @author jl
 * @since 2021-01-17 19:27:13
 */
public interface SellerInfoService extends IService<SellerInfo> {

    PageUtils queryPage(Map<String, Object> params);
}

