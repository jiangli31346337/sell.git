package cc.sunni.sell.service;

import cc.sunni.sell.dto.CartDto;
import cc.sunni.sell.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import cc.sunni.sell.entity.ProductInfo;

import java.util.List;
import java.util.Map;

/**
 * 商品表
 *
 * @author jl
 * @since 2021-01-17 19:27:13
 */
public interface ProductInfoService extends IService<ProductInfo> {

    PageUtils queryPage(Integer pageNo, Integer size, Map<String, Object> map);

    List<ProductInfo> getProductInfoUp();

    /**
     * 减库存
     */
    void decrStock(List<CartDto> cartDtos);

    /**
     * 加库存
     */
    void incrStock(List<CartDto> cartDtos);

    void onSale(String productId);

    void offSale(String productId);
}

