package cc.sunni.sell.service;

import cc.sunni.sell.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import cc.sunni.sell.entity.ProductCategory;
import java.util.Map;

/**
 * 商品类目
 *
 * @author jl
 * @since 2021-01-17 19:27:13
 */
public interface ProductCategoryService extends IService<ProductCategory> {

    PageUtils queryPage(Map<String, Object> params);
}

