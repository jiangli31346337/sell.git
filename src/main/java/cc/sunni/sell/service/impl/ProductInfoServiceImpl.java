package cc.sunni.sell.service.impl;

import cc.sunni.sell.dto.CartDto;
import cc.sunni.sell.dto.ProductDto;
import cc.sunni.sell.enums.ResultEnum;
import cc.sunni.sell.enums.SellEnums;
import cc.sunni.sell.exception.RRException;
import cc.sunni.sell.utils.PageUtils;
import cc.sunni.sell.utils.Query;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cc.sunni.sell.dao.ProductInfoDao;
import cc.sunni.sell.entity.ProductInfo;
import cc.sunni.sell.service.ProductInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("productInfoService")
public class ProductInfoServiceImpl extends ServiceImpl<ProductInfoDao, ProductInfo> implements ProductInfoService {

    @Override
    public PageUtils queryPage(Integer pageNo, Integer size, Map<String, Object> map) {
        Page<ProductDto> page = new Page<>(pageNo, size);
        List<ProductDto> list = this.baseMapper.queryPage(page,map);
        page.setRecords(list);
        return new PageUtils(page);
    }

    @Override
    public List<ProductInfo> getProductInfoUp() {
        return this.list(new LambdaQueryWrapper<ProductInfo>().eq(ProductInfo::getProductStatus, SellEnums.ProductStatusEnum.UP.getCode()));
    }

    @Override
    @Transactional
    public void decrStock(List<CartDto> cartDtos) {
        for (CartDto cartDto : cartDtos) {
            ProductInfo productInfo = this.baseMapper.selectById(cartDto.getProductId());
            if (productInfo == null) {
                throw new RRException(ResultEnum.PRODUCT_NOT_EXIST);
            }
            int i = this.baseMapper.decrStock(cartDto.getProductId(), cartDto.getProductQuantity());
            if (i < 0) {
                throw new RRException(ResultEnum.PRODUCT_STOCK_NOT_ENOUGH);
            }
        }
    }

    @Override
    public void incrStock(List<CartDto> cartDtos) {

    }

    @Override
    public void onSale(String productId) {
        ProductInfo productInfo = this.baseMapper.selectById(productId);
        if (productInfo == null) {
            throw new RRException(ResultEnum.PRODUCT_NOT_EXIST);
        }
        if (productInfo.getProductStatus().equals(SellEnums.ProductStatusEnum.UP.getCode())) {
            throw new RRException(ResultEnum.PRODUCT_STATUS_ERROR);
        }
        productInfo.setProductStatus(SellEnums.ProductStatusEnum.UP.getCode());
        this.baseMapper.updateById(productInfo);
    }

    @Override
    public void offSale(String productId) {
        ProductInfo productInfo = this.baseMapper.selectById(productId);
        if (productInfo == null) {
            throw new RRException(ResultEnum.PRODUCT_NOT_EXIST);
        }
        if (productInfo.getProductStatus().equals(SellEnums.ProductStatusEnum.DOWN.getCode())) {
            throw new RRException(ResultEnum.PRODUCT_STATUS_ERROR);
        }
        productInfo.setProductStatus(SellEnums.ProductStatusEnum.DOWN.getCode());
        this.baseMapper.updateById(productInfo);
    }

}