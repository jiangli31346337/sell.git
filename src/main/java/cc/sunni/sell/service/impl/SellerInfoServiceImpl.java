package cc.sunni.sell.service.impl;

import cc.sunni.sell.utils.PageUtils;
import cc.sunni.sell.utils.Query;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cc.sunni.sell.dao.SellerInfoDao;
import cc.sunni.sell.entity.SellerInfo;
import cc.sunni.sell.service.SellerInfoService;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service("sellerInfoService")
public class SellerInfoServiceImpl extends ServiceImpl<SellerInfoDao, SellerInfo> implements SellerInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SellerInfo> page = this.page(
                new Query<SellerInfo>().getPage(params),
                // 拼接查询条件
                new LambdaQueryWrapper<SellerInfo>()
        );

        return new PageUtils(page);
    }

}