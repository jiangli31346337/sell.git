package cc.sunni.sell.service.impl;

import cc.sunni.sell.utils.PageUtils;
import cc.sunni.sell.utils.Query;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cc.sunni.sell.dao.ProductCategoryDao;
import cc.sunni.sell.entity.ProductCategory;
import cc.sunni.sell.service.ProductCategoryService;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service("productCategoryService")
public class ProductCategoryServiceImpl extends ServiceImpl<ProductCategoryDao, ProductCategory> implements ProductCategoryService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductCategory> page = this.page(
                new Query<ProductCategory>().getPage(params),
                // 拼接查询条件
                new LambdaQueryWrapper<ProductCategory>()
        );

        return new PageUtils(page);
    }

}