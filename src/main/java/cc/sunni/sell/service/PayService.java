package cc.sunni.sell.service;

import cc.sunni.sell.dto.OrderDto;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;

/**
 * @author jl
 * @since 2021/1/20 20:37
 */
public interface PayService {

    PayResponse create(OrderDto orderDTO);

    PayResponse notify(String notifyData);

    RefundResponse refund(OrderDto orderDTO);
}
