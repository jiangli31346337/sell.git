## 有需要资料的小伙伴可以加群242733211
微信点餐系统
项目接口文档地址:https://docs.apipost.cn/view/006e9a8a4900f5c4
前端项目启动和部署
1.前端项目代码下载,地址:https://gitee.com/jiangli31346337/sell.git
2.npm install打包生成dist目录
3.启动nginx,将dist放到nginx的html/sell目录中
4.修改nginx.conf配置
 server {
        listen       80;
        server_name  sell.com;

        location / {
            root   html/sell;
            index  index.html index.htm;
        }
        
        location /sell/ {
            proxy_pass http://127.0.0.1:8080/sell/;
        }
	}
    注:前端代码和后端代码全部部署在本地机器
 配置hosts  127.0.0.1 sell.com
5.浏览器访问：http://127.0.0.1/#/order/，这是会出现空白界面，按F2打开浏览器的开发者工具，在浏览器的控制台输入document.cookie='openid=abc123' 向该域名下添加cookie。再次访问：http://127.0.0.1，这时就可以访问到前端界面了    
6.sell.com访问

## 知识点
**1.日志的使用**

**2.Lambda Stream Collectors.groupingBy()分组**

**3.enum的定义使用**

**4.renren的基础工具类**

**5.返回前端自定义属性名@JsonProperty**

**6.MP VO对象分页 OrderMasterServiceImpl#queryPage()**

**7.实体类非空检验及异常全局捕获和自定义校验**

**8.@JsonIgnore查询不会显示该属性,对象序列化时忽略该属性**

**9.MybatisPlus新的分页插件**

**10.WxJava WxOAuth2Service微信网页域名授权**

**11.EnumUtil.likeValueOf使用hutool的EnumUtil操作enum**

**12.freemarker的简单使用**

**13.使用aop校验用户是否登录**

**14.微信MP发送模板消息**

**15.全局异常处理中使用@ResponseStatus改变响应的状态码**

**16.websocket的使用**

**17.spring cache的使用**

**18.mybatis-plus更新null值PayServiceTest**
